import re
import threading
from flask import Flask
from laptops import laptops
from beverages import beverages
from sports import sports
from but_sold import but_sold



app = Flask(__name__)


@app.route('/mercadoLibre', methods=["GET"])
def mercadoLibre():
    hilo1 = threading.Thread(name='laptops', target=laptops, daemon=True)
    hilo2 = threading.Thread(name='beverages', target=beverages, daemon=True)
    hilo3 = threading.Thread(name='sports', target=sports, daemon=True)
    hilo4 = threading.Thread(name='but_sold', target=but_sold, daemon=True)
    
    hilo1.start()
    hilo2.start()
    hilo3.start()
    hilo4.start()
    return "Ejecutando scraping"


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
