# Scraping Mercado Libre

## Flask

[Flask](https://flask.palletsprojects.com/en/2.1.x/) es un marco web de Python pequeño y ligero que proporciona herramientas y funciones útiles que hacen que crear aplicaciones web en Python sea más fácil. Ofrece a los desarrolladores flexibilidad y un marco más accesible para los nuevos desarrolladores ya que puede crear una aplicación web rápidamente usando únicamente un archivo Python. Flask también es extensible y no fuerza una estructura de directorio concreta ni requiere código estándar complicado antes de iniciarse.

Como parte de este tutorial, usará el kit de herramientas Bootstrap para dar estilo a su aplicación, para que sea más atractiva visualmente. Bootstrap le ayudará a incorporar páginas web reactivas en su aplicación web para que también funcione bien en navegadores móviles sin tener que escribir su propio código HTML, CSS y JavaScript para conseguir estos objetivos. El kit de herramientas le permitirá centrarse en aprender cómo funciona Flask.

Flask utiliza el motor de plantillas Jinja para crear dinámicamente páginas HTML usando conceptos Python familiares como variables, bucles, listas, etcétera. Usará estas plantillas como parte de este proyecto.


## Beautiful

[Beautiful](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) Soup es una biblioteca de Python para extraer datos de archivos HTML y XML. Funciona con su analizador favorito para proporcionar formas idiomáticas de navegar, buscar y modificar el árbol de análisis. Comúnmente ahorra a los programadores horas o días de trabajo.

Estas instrucciones ilustran todas las funciones principales de Beautiful Soup 4, con ejemplos. Le muestro para qué sirve la biblioteca, cómo funciona, cómo usarla, cómo hacer que haga lo que quiere y qué hacer cuando viola sus expectativas.

Este documento cubre la versión 4.11.0 de Beautiful Soup. Los ejemplos de esta documentación se escribieron para Python 3.8.

Quizá estés buscando la documentación de Beautiful Soup 3 . Si es así, debe saber que Beautiful Soup 3 ya no se está desarrollando y que todo el soporte para él se eliminó el 31 de diciembre de 2020. Si desea conocer las diferencias entre Beautiful Soup 3 y Beautiful Soup 4, consulte Portabilidad de código para BS4 .

## lxml.etree

[lxml](https://lxml.de/apidoc/lxml.etree.html) es una biblioteca de Python que permite un fácil manejo de archivos XML y HTML, y también se puede utilizar para web scraping. Hay muchos analizadores XML disponibles en el mercado, pero para obtener mejores resultados, los desarrolladores a veces prefieren escribir sus propios analizadores XML y HTML. Aquí es cuando entra en juego la biblioteca lxml. Los beneficios clave de esta biblioteca son que es fácil de usar, extremadamente rápido al analizar documentos grandes, muy bien documentado y proporciona una conversión fácil de datos a tipos de datos de Python, lo que resulta en una manipulación de archivos más sencilla.

## Virtualvenv

Virtualenv es una herramienta usada para crear un entorno Python aislado. Este entorno tiene sus propios directorios de instalación que no comparten bibliotecas con otros entornos virtualenv o las bibliotecas instaladas globalmente en el servidor.

## Crear el entorno para la Api

Se requieren ciertos requisitos en el equipo si es que no se cuenta con ellos aun en la maquita. Uno de ellos es **virtualenv**, la cual se instala con los siguiente comando 
    
    python3 -m pip install --upgrade pip

    pip3 install virtualenv

Si no se cuenta aún con pip instalado o python se recomienda realizar la instalación en el equipo correspondiente, para este desarrollo se utilizaron las siguientes versiones:

    Python 3.10.4

    pip 22.0.4

* ### Proyecto
    Podrán descargar el repositorio del proyecto de la siguiente URL
    [scraping_test](https://gitlab.com/jonyacolin/scraping_test)
 
    Una vez clonado el repositorio y estando situado en la raíz del proyecto se creará el entorno virtual donde se instalarán las dependencias del proyecto para su posterior ejecución

        virtualven venv

    Una vez creado el entorno virtual se accede a él con el siguiente comando

        venv\Scripts\activate

    Una vez dentro del entorno virtual se ejecuta el siguiente comando para instalar las librerías del archivo **requeriments.txt**

        pip install -r .\requeriments.txt

    Al finalizar las instalaciones se tendrá lo necesario para iniciar la api a la cual se le realizará la siguiente petición para que proceda con la ejecución de la prueba. En mi caso estoy utilizando un sistema operativo Windows

        py appy.py
    
    **Petición:**

        http://0.0.0.0:5000/mercadoLibre


    Una vez realizada la petición comenzará a ejecutarse en la api el cual va a generar una archivo json llamado **Productos.json** el cual tendrá toda la información de los productos obtenidos de las siguientes páginas

    - https://laptops.mercadolibre.com.mx/laptops-accesorios/#menu=categories
    - https://listado.mercadolibre.com.mx/supermercado/bebidas/
    - https://listado.mercadolibre.com.mx/_Deal_deportes-y-fitness-accesorios
    - https://www.mercadolibre.com.mx/mas-vendidos/MLM1144

