import requests
import json
from bs4 import BeautifulSoup
from lxml import etree


def beverages():
    siguiente = 'https://listado.mercadolibre.com.mx/supermercado/bebidas/'
    for i in range(0,10) :
        response = requests.get(siguiente)
        if response.status_code == 200:
            soup = BeautifulSoup(response.content, 'html.parser')
            dom = etree.HTML(str(soup))
            urls = soup.find_all('a', attrs={"class": "ui-search-result__content ui-search-link"})
            urls = [i.get('href') for i in urls]
            for url in urls:
                product = parse_link(url)
                with open(f'Productos.json', 'a', encoding='utf-8') as f:
                    json.dump(product, f)
        else:
            break
        siguiente = dom.xpath(
            '//div[@class="ui-search-pagination"]/ul/li[contains(@class,"--next")]/a')[0].get('href')


def parse_link(url):
    response = requests.get(url)
    if response.status_code == 200:
        soup = BeautifulSoup(response.content, 'html.parser', from_encoding='utf8')
        dom = etree.HTML(str(soup))
        decription = dom.xpath('//div[@class="ui-pdp-header__title-container"]/h1/text()')
        if decription:
            decription = decription[0] 
        price = dom.xpath('//div[@class="ui-pdp-price__second-line"]/span/span[3]/text()')
        if price:
            price = price[0]
        old_price = dom.xpath('//div[@class="ui-pdp-price mt-16 ui-pdp-price--size-large"]/s/span[3]/text()')
        if old_price:
            old_price = old_price[0]
        brand = dom.xpath('//ol[@class="andes-breadcrumb"]/li[4]/a/@title')
        if brand:
            brand = brand[0]
        url_image = dom.xpath('//div[@class="ui-pdp-gallery__column"]/span[1]/figure/img/@src')
        if url_image:
            url_image = url_image[0]
        diccionary = ({"beverages":
                { 
                    "url": url, 
                    "descripcion": decription, 
                    "price": price, 
                    "old_price": old_price,
                    "brand": brand, 
                    "url_imagen": url_image
                }
            })
        return diccionary


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
